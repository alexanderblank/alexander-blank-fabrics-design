Alexander Blank Fabrics and design is a unique retail design showroom which encompasses a huge array of fabric choices from special stock program options to exclusive designer fabrics surrounded by a beautiful store, talented design staff, in house workroom and an extensive Hunter Douglas gallery.

Address: 2151 York Rd, Ste A, Lutherville-Timonium, MD 21093, USA

Phone: 410-561-2331

Website: https://www.alexblankfabrics.com